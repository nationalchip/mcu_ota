#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include "porting_uart.h"

#include "leo_mini-uart.h"
#include "mcu_nor.h"
#include "dsp.h"

#define DEBUG

#ifdef DEBUG
#define DBG(fmt, ...) printf ("[ DBG]" fmt, ##__VA_ARGS__)
#else
#define DBG(fmt, ...)
#endif

#define  ERR(fmt, ...) printf ("[ ERR]" fmt, ##__VA_ARGS__)
#define INFO(fmt, ...) printf ("[INFO]" fmt, ##__VA_ARGS__)

struct boot_header {
	uint16_t chip_id;
	uint8_t  chip_type;
	uint8_t  chip_version;

	uint16_t boot_delay;
	uint8_t  baudrate;
	uint8_t  reserved_1;

	uint32_t stage1_size;
	uint32_t stage2_baud_rate; // baudrate config when downloading stage2 and coming into stage2 cmdline
	uint32_t stage2_size;
	uint32_t stage2_checksum;
	uint8_t  resv[8];
}__attribute__((packed));

#define MAX_TIMEOUT_MS 10000
#define SERIAL_RESULT_HEADER "[Result]:"
#define START_FLAG  "~sta~"
#define FINISH_FLAG "~fin~"

enum {
	BOOT_STAGE_GET_BOOT_HEADER = 1,
	BOOT_STAGE_WAIT_ROM_BOOTUP,
	BOOT_STAGE_XFER_STAGE1,
	BOOT_STAGE_WAIT_STAGE1_BOOTUP,
	BOOT_STAGE_STAGE2_PREPARE,
	BOOT_STAGE_XFER_STAGE2,
	BOOT_STAGE_BOOT_XFER_DONE,
};

enum {
	DOWNLOAD_STAGE_SEND_COMMAND = 0x1,
	DOWNLOAD_STAGE_SEND_CRC,
	DOWNLOAD_STAGE_SEND_DATA_WAIT_START,
	DOWNLOAD_STAGE_SEND_DATA,
	DOWNLOAD_STAGE_SEND_DATA_WAIT_FINISH,
	DOWNLOAD_STAGE_WAIT_RESULT,
	DOWNLOAD_STAGE_FINISH,
	DOWNLOAD_STAGE_CLEAR,
};

#define BOOT_STAGE2_BAUDRATE 115200
#define BOOT_HEADER_LEN 32
#define BOOT_STAGE1_LEN (32*1024)
#define BOOT_BUF_LEN 64
#define BOOT_STAGE1_BAUD_GRUS 230400 // 大部分芯片都可以支持到 576000，不过处于通用的考虑，选择 230400
#define BOOT_STAGE2_BAUD_GRUS 1500000 // 可配置的范围 115200 ~ 1500000
#define SIZE_4K (0x1000)
#define MIN_DOWNLOAD_LEN (SIZE_4K)

/*
 * nc_msleep : 鐫＄湢
 */
void nc_msleep(int ms)
{
	usleep(1000 * ms);
	return ;
}

/*
 * nc_get_time_ms : 鑾峰彇褰撳墠绯荤粺鏃堕棿
 */
uint64_t nc_get_time_ms(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);
	return (tv.tv_sec*1000 + tv.tv_usec/1000);
}

/*
 * nc_reboot_chip : 閲嶅惎 leo/leo mini
 */
void nc_reboot_chip(void)
{
	printf ("please reboot the board\n");
	return ;
}

static uint8_t* get_boot_file_from_outside(uint32_t len)
{
	static uint8_t *boot_file;
	static uint32_t boot_file_len;
	static int init, offset;
	uint8_t *ret;

	if (!init) {
                 boot_file = leo_mini_uart_boot;//leo_uart_boot;
                 boot_file_len = leo_mini_uart_boot_len;//leo_uart_boot_len;
		init = 1;
		offset = 0;
	}

	if (offset >= boot_file_len)
		return NULL;

	ret     = boot_file + offset;
	offset += len;
	return ret;
}

typedef enum {
	CHIP_NONE = 0,
	CHIP_LEO,
	CHIP_LEO_MINI,
	CHIP_GRUS,
} chip_t;

typedef enum {
	CHIP_ID_GX8010 = 0x8010, // leo
	CHIP_ID_GX8008 = 0x8008, // leo mini
	CHIP_ID_GX8002 = 0x8002, // grus
} chip_id_t;

typedef enum {
	CHIP_VERSION_V1 = 0x01,
} chip_version_t;

static int32_t is_little_endian(void)
{
	uint32_t a = 0x11223344;
	uint8_t *p = (unsigned char *)&a;

	return (*p == 0x44) ? 1 : 0;
}

static uint32_t switch_endian(uint32_t v)
{
	return (((v >> 0) & 0xff) << 24) | (((v >> 8) & 0xff) << 16) | (((v >> 16) & 0xff) << 8) | (((v >> 24) & 0xff) << 0);
}

uint32_t be32_to_cpu(uint32_t v)
{
	if (is_little_endian())
		return switch_endian(v);
	else
		return v;
}

static int uart_wait_strings(const char *str, int timeout_ms)
{
	int i;
	int stringlen = strlen(str);
	uint64_t start_time, current_time, remain_time;
	uint8_t ch;

	start_time = nc_get_time_ms();
	current_time = start_time;
	while (current_time < start_time + timeout_ms) {
		remain_time = start_time + timeout_ms - current_time;
		for (i = 0; i < stringlen; i++) {
			if (nc_uart_read_nonblock(&ch, 1, remain_time) == 1) {
				// printf ("%c", ch);
				if (ch == '\r')
					ch = '\n';
				if (ch != str[i])
					break;
			} else {
				break;
			}

			if (i + 1 == stringlen)
				return 0;
		}
		current_time = nc_get_time_ms();
	}

	return -1;
}

int uart_sendboot_streaming(uint8_t *boot, int len)
{
	static struct boot_header boot_header_static;
	static int recv_offset, stage1_size, stage2_size;
	static int cur_status = BOOT_STAGE_GET_BOOT_HEADER;

	int baudrate[] = {300, 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
	unsigned int stage2_baud_rate = BOOT_STAGE2_BAUD_GRUS;;
	struct boot_header *boot_header;
	unsigned char wait_char, get_char;
	unsigned char send_char = 0xef;
	uint8_t buf[BOOT_BUF_LEN];
	uint32_t checksum = 0;
	int copy_len, send_len, offset;
	int ret;
	int i = 0;

    static chip_t chip;

	boot_header = &boot_header_static;

	if (!boot || len <= 0)
		return -1;

	offset = 0;
	while (len > 0) {
		switch (cur_status) {
			case BOOT_STAGE_GET_BOOT_HEADER:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				copy_len = len + recv_offset >= sizeof(struct boot_header) ? (sizeof(struct boot_header) - recv_offset) : (len);
				memcpy((uint8_t *)boot_header + recv_offset, boot + offset, copy_len);
				recv_offset   += copy_len;
				offset        += copy_len;
				len           -= copy_len;
				if (recv_offset >= sizeof(struct boot_header)) {
					cur_status = BOOT_STAGE_WAIT_ROM_BOOTUP;
					recv_offset = 0;
				}
				break;
			}
			case BOOT_STAGE_WAIT_ROM_BOOTUP:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				switch (boot_header->chip_id){
				case CHIP_ID_GX8008:
				case CHIP_ID_GX8002:
					if (boot_header->chip_id == CHIP_ID_GX8008)
						chip = CHIP_LEO_MINI;
					else
						chip = CHIP_GRUS;

					if (boot_header->chip_version == CHIP_VERSION_V1) {
						stage1_size = be32_to_cpu(boot_header->stage1_size);
						wait_char = 'M';
					} else {
						ERR("Unsupported chip version!\n");
						return -1;
					}
					break;
				case CHIP_ID_GX8010:
					chip = CHIP_LEO;
					if (boot_header->chip_version == CHIP_VERSION_V1) {
						stage1_size = BOOT_STAGE1_LEN;
						wait_char = 'M';
					} else {
						ERR("Unsupported chip version!\n");
						return -1;
					}
					break;
				default:
					ERR("Unsupported chip id!\n");
					return -1;
				}

				switch (chip) {
				case CHIP_LEO_MINI:
				case CHIP_GRUS:
					if (nc_uart_config(BOOT_STAGE1_BAUD_GRUS, 8, 1, 'N') < 0) {
						ERR("Uart config error!\n");
						return -1;
					}
					break;
				default:
					if (nc_uart_config(baudrate[boot_header->baudrate], 8, 1, 'N') < 0) {
						ERR("Uart config error!\n");
						return -1;
					}
					break;
				}

				nc_reboot_chip();

				while (1) {
					ret = nc_uart_try_read(&get_char, 1);
					if (ret == 1)
					{
						if (get_char == wait_char)
							break;
					}
					// for leo mini
					nc_uart_write(&send_char, 1);
					nc_msleep(1);
				}

				stage1_size = stage1_size / 4;
				buf[0] = 'Y';
				buf[1] = (stage1_size >>  0) & 0xFF;
				buf[2] = (stage1_size >>  8) & 0xFF;
				buf[3] = (stage1_size >> 16) & 0xFF;
				buf[4] = (stage1_size >> 24) & 0xFF;
				nc_uart_write(buf, 5);

				stage1_size *= 4;
				cur_status = BOOT_STAGE_XFER_STAGE1;
				break;
			}
			case BOOT_STAGE_XFER_STAGE1:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				/* Send boot stage1 */
				if (boot_header->chip_type == 0x01) {
						send_len = (len + recv_offset) >= stage1_size ? (stage1_size - recv_offset) : len;
						ret = nc_uart_write(boot + offset, send_len);
						if (ret != send_len) {
							ERR ("uart write failed! except : %d actual : %d\n", send_len, ret);
							return -1;
						}

						len         -= send_len;
						offset      += send_len;
						recv_offset += send_len;

						if (recv_offset < stage1_size)
							break;

						while (1) {
							ret = nc_uart_try_read(&get_char, 1);
							if (ret == 1) {
								DBG ("recv : %x\n", get_char);
								if (get_char == 'E' || get_char == 'F') {
									if (get_char == 'E') {
										cur_status = BOOT_STAGE_BOOT_XFER_DONE;
										ERR ("CRC FAILED, recv : %c %x\n", get_char, get_char);
										return -1;
									} else {
										DBG ("CRC SUCCESS\n");
										cur_status = BOOT_STAGE_WAIT_STAGE1_BOOTUP;
										recv_offset = 0;
										break;
									}
								}
							}
						}
				} else {
					DBG(" %s %d \n", __func__, __LINE__);
					ERR("Unsupported chip type!\n");
					return -1;
				}

				break;
			}
			case BOOT_STAGE_WAIT_STAGE1_BOOTUP:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				/* Send boot stage2 */
				switch (chip) {
					case CHIP_LEO_MINI:
					case CHIP_GRUS:
						// wait for stage1 to start
						if (uart_wait_strings("wfb", 4000) != 0) {
							ERR ("Wait for \"wfb\",please check UART receive !\n");
							return -1;
						}

						// ACK
						sprintf ((char *)buf, "OK");
						nc_uart_write(buf, strlen((char *)buf));

						buf[0] = (stage2_baud_rate >>  0) & 0xff;
						buf[1] = (stage2_baud_rate >>  8) & 0xff;
						buf[2] = (stage2_baud_rate >> 16) & 0xff;
						buf[3] = (stage2_baud_rate >> 24) & 0xff;
						nc_uart_write(&buf[0], 4);

						// Wait for the completion of the previous data transmission
						if (uart_wait_strings("OK", 4000) != 0) {
							ERR ("Wait for \"OK\",please check UART receive !\n");
							cur_status = BOOT_STAGE_BOOT_XFER_DONE;
							return -1;
						}

						DBG ("baud :  %d\n", stage2_baud_rate);
						if (nc_uart_config(stage2_baud_rate, 8, 1, 'N') < 0) {
							ERR("Uart config error!\n");
							return -1;
						}
						break;

					default:
						/* Wait until boot stage1 send complete */
						for (i = 0; i < boot_header->boot_delay; i++)
							nc_msleep(1);

						/* Send boot stage2 */
						if (nc_uart_config(BOOT_STAGE2_BAUDRATE, 8, 1, 'N') < 0) {
							ERR("Uart config error!\n");
							return -1;
						}
						break;
				}

				if (uart_wait_strings("GET", 4000) < 0) {
					ERR("Wait 'GET' string error!\n");
					cur_status = BOOT_STAGE_BOOT_XFER_DONE;
					return -1;
				}

				switch (chip) {
					case CHIP_LEO_MINI:
					case CHIP_GRUS:
						sprintf ((char *)buf, "OK");
						nc_uart_write(buf, strlen((char *)buf));
						break;
					default:
						break;
				}

				DBG ("stage1 finished\n");
				buf[0] = 'S';
				nc_uart_write(buf, 1);
				cur_status = BOOT_STAGE_STAGE2_PREPARE;
				break;
			}
			case BOOT_STAGE_STAGE2_PREPARE:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				switch (chip) {
					case CHIP_LEO_MINI:
					case CHIP_GRUS:
						stage2_size = be32_to_cpu(boot_header->stage2_size);
						checksum    = be32_to_cpu(boot_header->stage2_checksum);
						break;
					default:
						// for (i = 0, checksum = 0; i < stage2_size; i++)
						// 	checksum += pdata[stage2_offset + i];
						return -1;
				}

				if (boot_header->chip_type == 0x01) {
					nc_uart_write((uint8_t *)&checksum, 4);
					nc_uart_write((uint8_t *)&stage2_size, 4);
				} else {
					ERR("Unsupported chip type!\n");
					return -1;
				}

				switch (chip) {
					case CHIP_LEO_MINI:
					case CHIP_GRUS:
						// 由于下载速率的提高，pc 需要等待 mcu 准备好 dma 后，再发送数据，不然 mcu 会来不及处理
						if (uart_wait_strings("ready", 1000) < 0) {
							ERR("Wait ready string error!\n");
							cur_status = BOOT_STAGE_BOOT_XFER_DONE;
							return -1;
						}
						break;
					default:
						break;
				}
				cur_status = BOOT_STAGE_XFER_STAGE2;
				break;
			}
			case BOOT_STAGE_XFER_STAGE2:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				send_len = len + recv_offset >= stage2_size ? (stage2_size - recv_offset) : len;
				ret = nc_uart_write(boot + offset, send_len);
				if (ret != send_len) {
					ERR ("uart send failed, expect : %d, actual : %d\n", send_len, ret);
					return -1;
				}

				len         -= send_len;
				offset      += send_len;
				recv_offset += send_len;

				if (recv_offset < stage2_size)
					break;

				nc_uart_read(buf, 1);
				if (buf[0] == 'O') {
					cur_status = BOOT_STAGE_BOOT_XFER_DONE;
				} else if (buf[0] == 'E') {
					ERR("stage2 verify failed!\n");
					cur_status = BOOT_STAGE_BOOT_XFER_DONE;
					return -1;
				} else {
					ERR("Unknown character! : %x %c\n", buf[0], buf[0]);
					cur_status = BOOT_STAGE_BOOT_XFER_DONE;
					return -1;
				}

				if (uart_wait_strings("boot> ", 3000) < 0) {
					ERR("Wait 'boot> ' string error!\n");
					cur_status = BOOT_STAGE_BOOT_XFER_DONE;
					return -1;
				}

				DBG ("stage2 finished\n");
				break;
			}
			case BOOT_STAGE_BOOT_XFER_DONE:
			{
				DBG("<%d/%d> %d\n", cur_status, BOOT_STAGE_BOOT_XFER_DONE, len);
				memset(&boot_header_static, 0, sizeof(struct boot_header));
				recv_offset = 0;
				stage1_size = stage2_size = 0;
				cur_status = BOOT_STAGE_GET_BOOT_HEADER;
				break;
			}
			default:
				ERR ("error status : %d\n", cur_status);
				break;
		}
	}

	return 0;
}

static int download_bootfile_streaming(void)
{
    uint8_t  *data;
	uint32_t  each_size, left_size;
	int ret;

	INFO("start download bootfile\n");

	// We have now determined the boot file size
	left_size  = leo_mini_uart_boot_len;//116104;
    printf("left_size = %d\n", left_size);
	each_size = 512; // can be any length, such as:256,512,1024,2046,4096

    while (1) {
		if (each_size > left_size)
			each_size = left_size;

		// This can be replaced by receiving data from outside
		data = get_boot_file_from_outside(each_size);
	    ret = uart_sendboot_streaming(data, each_size);

		if (ret != 0) {
			INFO ("download boot file failed\n");
			return -1;
		}

		left_size -= each_size;
		if (left_size == 0)
			break;
	}

	INFO ("bootfile download finish\n");
    return 0;
}

typedef struct {
	uint32_t  addr;
	uint8_t  *buff;
	uint32_t  buff_len;
	uint32_t  crc;
} fw_t;

typedef struct {
    int32_t   fw_num;
    fw_t      fw[0];
} fw_info_t;

static uint8_t *get_data_from_outside(fw_info_t *info, uint32_t len)
{
	static uint32_t index, offset;
	uint8_t *ret;

	if (index >= info->fw_num) {
		index = 0;
		return NULL;
	}

	ret     = info->fw[index].buff + offset;
	offset += len;

	if (offset >= info->fw[index].buff_len) {
		index++;
		offset = 0;
	}

	return ret;
}

int32_t uart_download_streaming(fw_info_t *info, uint8_t *data, uint32_t len)
{
	char  uart_cmd[128];
	uint8_t *buff;
	uint32_t addr, offset;
	int32_t total_size, each_size;
	int32_t ret;
	int32_t i;
	uint32_t crc_cal;

	static int index, recv_offset, each_offset, cycle_num = -1;
	static int cur_status = DOWNLOAD_STAGE_SEND_COMMAND;

	// check valid
	if (info->fw_num <= 0) {
		ERR("invalid fw info number : %d\n", info->fw_num);
		return -1;
	}

	for (i = 0; i < info->fw_num; i++) {
		addr       = info->fw[i].addr;
		buff       = info->fw[i].buff;
		total_size = info->fw[i].buff_len;

		if (addr % MIN_DOWNLOAD_LEN || buff == NULL || total_size <= 0) {
			ERR("exist invalid buff addr or buff len!! index:%d\n", i);
			return -1;
		}
	}

	each_size = 512 * 1024;

	offset = 0;
	while (len > 0) {
		switch (cur_status) {
			case DOWNLOAD_STAGE_SEND_COMMAND:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				if (index >= info->fw_num) {
					ERR("noting to download\n");
					return -1;
				}

				// init
				recv_offset = each_offset = 0;
				cycle_num  = -1;

				addr       = info->fw[index].addr;
				total_size = info->fw[index].buff_len;
				memset(uart_cmd, 0, sizeof(uart_cmd));
				// serial down cmd : serialdown offset len each_size
				sprintf(uart_cmd, "serialdown 0x%x %d %d\n", addr, total_size, each_size);
				DBG ("uart cmd : %s", uart_cmd);
				ret = nc_uart_write((const uint8_t *)uart_cmd, strlen(uart_cmd));
				if (ret != strlen(uart_cmd)) {
					ERR ("uart send failed\n");
					return -1;
				}
				cur_status = DOWNLOAD_STAGE_SEND_CRC;
				break;
			}
			case DOWNLOAD_STAGE_SEND_CRC:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				crc_cal = info->fw[index].crc;
				ret = nc_uart_write((uint8_t *)&crc_cal, sizeof(crc_cal));
				if (ret != sizeof(crc_cal)) {
					ERR("uart send crc failed, ret : %d\n", ret);
					return -1;
				}
				cur_status = DOWNLOAD_STAGE_SEND_DATA_WAIT_START;
				break;
			}
			case DOWNLOAD_STAGE_SEND_DATA_WAIT_START:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				if (cycle_num == -1) {
					total_size = info->fw[index].buff_len;
					cycle_num = total_size / each_size;
					if (total_size % each_size)
						cycle_num++;
				}

				// start
				ret = uart_wait_strings(START_FLAG, MAX_TIMEOUT_MS);
				if (ret < 0) {
					ERR ("wait %s failed, cmd inde : %d\n", START_FLAG, i);
					return -1;
				}
				cur_status = DOWNLOAD_STAGE_SEND_DATA;
				break;
			}
			case DOWNLOAD_STAGE_SEND_DATA:
			{
				uint32_t send_len;

				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				total_size = info->fw[index].buff_len;
				send_len = len + each_offset >= each_size ? (each_size - each_offset) : len;
				ret = nc_uart_write(data + offset, send_len);
				if (ret != send_len) {
					ERR ("uart write failed, except : %d, actual : %d\n", send_len, ret);
					return -1;
				}

				len         -= send_len;
				offset      += send_len;
				recv_offset += send_len;
				each_offset += send_len;
				// printf ("%x %x %x %x %x\n", len, offset, recv_offset, each_offset, send_len);

				if (recv_offset >= total_size) {
					recv_offset = 0;
					cur_status  = DOWNLOAD_STAGE_SEND_DATA_WAIT_FINISH;
				} else if (each_offset >= each_size) {
					each_offset = 0;
					cur_status  = DOWNLOAD_STAGE_SEND_DATA_WAIT_FINISH;
				} else
					break;

				// break; go through
			}
			case DOWNLOAD_STAGE_SEND_DATA_WAIT_FINISH:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);

				// finish
				ret = uart_wait_strings(FINISH_FLAG, MAX_TIMEOUT_MS);
				if (ret < 0) {
					ERR ("wait %s failed, cmd index : %d\n", FINISH_FLAG, index);
					return -1;
				}

				cycle_num--;
				if (cycle_num) {
					cur_status = DOWNLOAD_STAGE_SEND_DATA_WAIT_START;
					break;
				}

				cur_status = DOWNLOAD_STAGE_WAIT_RESULT;
				// break; go through
			}

			case DOWNLOAD_STAGE_WAIT_RESULT:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				if (uart_wait_strings(SERIAL_RESULT_HEADER, 10000) != 0 || uart_wait_strings("SUCC", 4000) != 0) {
					ERR ("download failed\n");
					return -1;
				}

				index++;
				if (index < info->fw_num) {
					cur_status = DOWNLOAD_STAGE_SEND_COMMAND;
					break;
				}

				cur_status = DOWNLOAD_STAGE_FINISH;
				// break; go through
			}
			case DOWNLOAD_STAGE_FINISH:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
	#ifdef REBOOT_AFTER_DOWNLOAD_FINISH
				// reboot
				memset(uart_cmd, 0, sizeof(uart_cmd));
				sprintf(uart_cmd, "reboot\n");
				ret = nc_uart_write((const uint8_t *)uart_cmd, strlen(uart_cmd));
				if (ret != strlen(uart_cmd)) {
					ERR ("send reboot cmd failed\n");
					return -1;
				}
				printf ("reboot finish\n");
	#endif
				cur_status = DOWNLOAD_STAGE_CLEAR;
			}
			case DOWNLOAD_STAGE_CLEAR:
			{
				DBG("<%d/%d> %d\n", cur_status, DOWNLOAD_STAGE_CLEAR, len);
				index = 0;
				cur_status = DOWNLOAD_STAGE_SEND_COMMAND;
				break;
			}
			default:
				break;
		}
	}

	return 0;
}

static int prepare_fw(fw_info_t **info_ret)
{
	fw_info_t *info;

	// prepare fw
	info = malloc(sizeof(int) + 2 * sizeof(fw_t));
	if (!info) {
		ERR ("info malloc failed\n");
		return -1;
	}

	info->fw[0].addr     = 0x0;
	info->fw[0].buff     = mcu_nor_bin;
	info->fw[0].buff_len = mcu_nor_bin_len;
	info->fw[0].crc      = crc32(0, info->fw[0].buff, mcu_nor_bin_len);

	info->fw[1].addr     = 0x20000;
	info->fw[1].buff     = dsp_fw;
	info->fw[1].buff_len = dsp_fw_len;
	info->fw[1].crc      = crc32(0, info->fw[1].buff, dsp_fw_len);

	info->fw_num         = 2;

	*info_ret = info;
	return 0;
}

static int download_fw_streaming(void)
{
#define MIN_DOWNLOAD_LEN_INN (/*2 * SIZE_4K_INN*/512) // This is because Flash has a minimum erasure length of 4k
	fw_info_t *info;
	uint8_t   *data;
	uint32_t   each_size, left_size;
	int ret;
	int i;

	INFO("start download firmware\n");

	// get download config, include [download count] [download addr] [len] [crc]
	if (prepare_fw(&info))
		return -1;

	i = 0;
	while (i < info->fw_num) {
		// get current download content length
		left_size = info->fw[i].buff_len;
		each_size = MIN_DOWNLOAD_LEN_INN;

		while (1) {
			if (each_size > left_size)
				each_size = left_size;

			// This can be replaced by receiving data from outside
			data = get_data_from_outside(info, each_size);
			ret = uart_download_streaming(info, data, each_size);

			if (ret != 0)
				goto failed;

			left_size -= each_size;
			if (left_size == 0) {
				INFO ("process : %d/%d\n", i+1, info->fw_num);
				break;
			}
		}

		i++;
	}

	INFO("firmware download finish\n");
	return 0;

failed:
	INFO("firmware download failed\n");
	return -1;
}

int main(int argc, char **argv)
{
    nc_uart_init();

    if (download_bootfile_streaming())
		return -1;

    if (download_fw_streaming())
        return -1;

    return 0;
}


